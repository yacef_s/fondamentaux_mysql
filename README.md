# Stage 00: Savoir importer une base de donnée

## Consignes
Jeune étudiant, le professeur vous a fourni votre Pokedex mais il a oublié d'y entrer les données.

Vous allez donc commencer par y importer pokedex.sql.

Pour être sûr que cela fonctionne, vous allez commencer par afficher tous les [identifier] dans la table pokemon.

Si tout se passe bien, vous remarquerez que la base de données est en anglais.

Pour la suite, je vous donnerai donc les infos des noms et/ou objets en anglais, entre parenthèses pour qu'il n'y ait pas de surprise.

# Stage 01:

**Utilisation du mot clés IN**

## Consignes 
Après avoir reçu votre Pokedex, il faut choisir votre starter. Faites un SELECT de leur [identifier] dans la table pokemon. Ils ont les id 1, 4, 7.

Résultat attendu :

    | identifier |
    | bulbasaur  |
    | charmander |
    | squirtle   |

# Stage 02:

**rechercher par chaîne de caractère**

## Consignes 

Vous savez que c'est ici que vous allez pouvoir trouver votre premier Pikachu (Pikachu). Faites un SELECT de toutes les informations possibles dans la table pokemon qui correspondent à Pikachu.

Résultat attendu :

| id  | identifier | species_id | height | weight | base_experience | order | is_default |
| 25  | pikachu    |       25   |   4    |   60   |       112       |  32   |      1     |


# Stage 03:

**Faire des calcules dans une requête SQL
Mettre un alias sur une colonne**

## Consignes 

Vous voilà contre Pierre pour essayer de gagner votre premier badge. Contre vous il utilise un Racaillou level 12 et un Onyx level 14. Faites une requête qui fait la somme des deux niveaux.

Faites bien attention au nom de la colonne dans le résultat. Regardez bien l'exemple.

Résultat attendu:

    |  resultat |
    |     26    |


# Stage 04:

**Recherche par encadrement d'id**

## Consignes 

Vous rencontrez enfin Léo, le scientifique et spécialiste des Evoli (Eevee). Pour l'aider faites une requête qui SELECT Evoli et ses évolutions qui ont leurs id entre 133 et 136 compris.

Résultat attendu :

    |  identifier  |
    |    eevee     |
    |    vaporeon  |
    |    jolteon   |
    |    flareon   |




# Stage 05:

**Joindre des tables ensemble**

## Consignes

Après moultes périples dans la tour de Lavanville vous avez enfin trouvé la Pokéflute. Avant de réveiller Ronflex (Snorlax), faites un listing des 5 premières attaques qui lui sont attribuées.

Pour cela vous aurez besoin de 3 tables : pokemon, moves et pokemon_moves.

Attention au nom de la colonne de résultat

Résultat attendu :

    |   attaque       |
    |    mega-punch   |
    |    pay-day      |
    |    mega-kick    |
    |    headbutt     |
    |    body-slam    |



# Stage 06:

**Joindre plusieurs tables ensemble**

## Consignes
Vous voilà arrivé à Parmanie, la ville du Safari. Vous pouvez attraper un Insecateur (Scyther). Mais vous ne vous rappellez plus de son type.

Pour cela vous aurez besoin de 3 tables : pokemon, types, et pokemon_types.

Attention au nom de la colonne de résultat

Résultat attendu :

    |   type      |
    |    flying   |
    |    bug      |




















